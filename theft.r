library(docopt)
#library(ks)
library(cvTools)
library(data.table)

'Usage:
gunshots.r [--S samplelocations] [--start startdate] [--end enddate]

Options:
--S samplelocations [default: 1000]
--start start date [default: 0]
--end end date [default: 84]
]' -> doc

opts <- docopt(doc)

nsamplelocations = as.numeric(opts$S)
startdate = as.numeric(opts$start)
enddate = as.numeric(opts$end)

library(kernlab)
library(MASS)
library(Matrix)
library(klin)
library(mgcv)
library(pdist)
#source("utils.r")
opts$M = as.numeric(opts$M)

library(doParallel)
registerDoParallel(cores=12)
source("utils.r")

load("theft.rdata")

pp = data.matrix(crime[crime$date >= startdate & crime$date < enddate,])
#pp = as.matrix(xyt[data$date >= startdate & data$date < enddate,])
unitsquare = function(x) {
  (x - min(x)) / (max(x) - min(x))
}
pp[,3] = unitsquare(pp[,3])
pp[,1] = (pp[,1] - min(pp[,1]))/137585
pp[,2] = (pp[,2] - min(pp[,2]))/137585
#for(i in 1:3) print(range(pp[,i]))

se.periodic = function(dd,lengthscale,period) {
  return((exp(-2*sin(dd*pi*period)^2)+1) * exp(-.5 * dd^2/lengthscale^2))
}

ll.wrapper = function(a,b,c,d) {
  result = ll(a,b,c,d)
  if(!is.finite(result))
    result = 1e8
  return(result)
}

st.base.kernel = function(x,u,lengthscale.s,lengthscale.t,mu,gamma,period,lowrank=30) {
  K.uu = se.periodic(as.matrix(dist(u[,3])),lengthscale.t,period) * se.k(as.matrix(dist(u[,1:2])), lengthscale.s)
  K.xu = se.periodic(as.matrix(pdist(as.matrix(x[,3]),as.matrix(u[,3]))),lengthscale.t,period) * 
    se.k(as.matrix(pdist(x[,1:2],u[,1:2])),lengthscale.s)
  K.xx = se.periodic(as.matrix(dist(as.matrix(x[,3]))),lengthscale.t,period) * 
    se.k(as.matrix(dist(x[,1:2])),lengthscale.s)
  return(list(K.uu=K.uu,
              K.xu=K.xu,
              K.xx=K.xx))
}
st.kernel = function(x,u,lengthscale.s,lengthscale.t,mu,gamma,period,lowrank=30) {
  K.uu = se.periodic(as.matrix(dist(u[,3])),lengthscale.t,period) * se.k(as.matrix(dist(u[,1:2])), lengthscale.s)
  K.xu = se.periodic(as.matrix(pdist(as.matrix(x[,3]),as.matrix(u[,3]))),lengthscale.t,period) * 
    se.k(as.matrix(pdist(x[,1:2],u[,1:2])),lengthscale.s)
  e = slanczos(K.uu,lowrank)
  m = nrow(u)
  return(list(K.xxreg=K.xu %*% e$vectors %*% diag(1/(mu/m * e$values^2 + gamma * e$values))%*% t(e$vectors) %*% t(K.xu),
              K.xureg=t(solve((mu/m * K.uu + gamma * diag(m)),t(K.xu)))))
}

rkhs = function(xtrain, xtest, samplelocations,mu,gamma,lengthscale.s,lengthscale.t,period) {
  Klist=st.kernel(rbind(xtrain,xtest),samplelocations,lengthscale.s,lengthscale.t,mu,gamma,period)
  ntrain = nrow(xtrain); ntest = nrow(xtest); n = ntrain + ntest
  alphastar = optim(rep(1,ntrain),function(alpha) ll.wrapper(alpha,Klist$K.xxreg[1:ntrain,1:ntrain],mu,1),
                    function(alpha) ll.grad(alpha,Klist$K.xxreg[1:ntrain,1:ntrain],mu,1),method="L-BFGS-B")
  
  ihat = mu*(t(alphastar$par) %*% Klist$K.xureg[1:ntrain,])^2
  
  ppl = -sum(log(mu * (Klist$K.xxreg[(ntrain+1):n,1:ntrain] %*% alphastar$par)^2)) + mean(ihat) 
  return(data.frame(lengthscale.s=lengthscale.s,lengthscale.t=lengthscale.t,mu=mu,gamma=gamma,period=period,ll=alphastar$value,ppl=ppl))
}
rkhs.naive = function(xtrain, xtest, samplelocations,mu,gamma,lengthscale.s,lengthscale.t,period) {
  Klist=st.base.kernel(rbind(xtrain,xtest),samplelocations,lengthscale.s,lengthscale.t,mu,gamma,period)
  ntrain = nrow(xtrain); ntest = nrow(xtest); n = ntrain + ntest
  
  R = 1/nrow(samplelocations) * Klist$K.xu[1:ntrain,] %*% t(Klist$K.xu[1:ntrain,])
  alphastar = optim(rep(1,ntrain),function(alpha) ll.naive(alpha,Klist$K.xx[1:ntrain,1:ntrain],R,mu,gamma),
                    function(alpha) ll.grad.naive(alpha,Klist$K.xx[1:ntrain,1:ntrain],R,mu,gamma),method="L-BFGS-B")
  ppl = (-sum(log(mu * (Klist$K.xx[(ntrain+1):n,1:ntrain] %*% alphastar$par)^2)) + 
           ( mu * t(alphastar$par) %*% R %*% alphastar$par))
  ihat = mu*(t(alphastar$par) %*% Klist$K.xu[1:ntrain,])^2
  
  return(data.frame(lengthscale.s=lengthscale.s,lengthscale.t=lengthscale.t,mu=mu,gamma=gamma,period=period,ll=alphastar$value,ppl=ppl))
}

samplelocations = as.matrix(replicate(3,runif(nsamplelocations)))

overall.results = NULL



folds = cvFolds(nrow(pp),2)
pp.train = as.matrix(pp[folds$subsets[folds$which == 1],])
pp.test = as.matrix(pp[folds$subsets[folds$which == 2],])

samplelocations = data.frame(0,0,rep(seq(0,1,length.out=(enddate-startdate) ),20))
samplelocations[,1] = runif(nrow(samplelocations))
samplelocations[,2] = runif(nrow(samplelocations))
samplelocations = data.matrix(samplelocations)
nrow(samplelocations)

# from aistats paper: c(.1,2,4,enddate/7,enddate) 

result = foreach(period = c(.1,2,4,6,7,12,14,21,28,42,84),.combine='rbind', .errorhandling='pass') %dopar% {
  ptm = proc.time() 
  r = rkhs(pp.train, pp.test, samplelocations, 10, 1, .2, .5, period) 
  r$elapsed = (proc.time() - ptm)[3]
  print(r)
  return(r)
  
}
result.naive = foreach(period = c(.1,2,4,6,7,12,14,21,28,42),.combine='rbind', .errorhandling='pass') %dopar% {
  ptm = proc.time() 
  r = rkhs.naive(pp.train, pp.test, samplelocations, 10, 1, .2, .5, period) 
  r$elapsed = (proc.time() - ptm)[3]
  print(r)
  return(r)
}

print(result)
print(result.naive)
