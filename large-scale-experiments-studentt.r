library(docopt)
library(ks)
library(cvTools)
library(data.table)
library(pdist)
library(clusterGeneration)

'Usage:
   large-scale-experiments-studentt.r [-D dimensionality] [-S samplelocations] [-k mixture-components] [--mu overall-intensity]  [--df df]  [--nreps nreps]

Options:
   -D dimensionality [default: 2]
   -k mixture-components [default: 10]
   --mu overall-intensity [default: 2]
   --df df [default: 5]
   --nreps nreps [default: 20]
   -S samplelocations [default: 100]
   -NT number of test locations [default: 100]
 ]' -> doc

opts <- docopt(doc)
k = as.numeric(opts$k); mu = as.numeric(opts$mu); D = as.numeric(opts$D) ; df = as.numeric(opts$df); nreps = as.numeric(opts$nreps)
nsamplelocations = as.numeric(opts$S)
ntestlocations = as.numeric(opts$NT)
#print(opts)

library(kernlab)
library(doParallel)
library(MASS)
registerDoParallel(cores=16)
source("utils.r")

## this is the unpenalized version of what we're learning by Empirical Risk Minimization
my.likelihood = function(K, R,mu,alpha) {
      (-sum(log(mu * (K %*% alpha)^2)) + ( mu * t(alpha) %*% R %*% alpha))
}

my.kernel1 = function(x,u,lengthscale,mu,gamma,lowrank=10,kernel=se.k) {
  K.uu = kernel(as.matrix(dist(u)),lengthscale)
  K.xu = kernel(as.matrix(pdist(x,u)),lengthscale)
  #K.xt = kernel(as.matrix(pdist(x,testlocations)),lengthscale)
  #K.tt = kernel(as.matrix(dist(testlocations)),lengthscale)
  #e = eigen(K.uu) #
  e = slanczos(K.uu,lowrank)
  m = nrow(u)
  return(list(K.xxreg=K.xu %*% e$vectors %*% diag(1/(mu/m * e$values^2 + gamma * e$values))%*% t(e$vectors) %*% t(K.xu),
              K.xureg=t(solve((mu/m * K.uu + gamma * diag(m)),t(K.xu)))))
            #  K.xtreg=t(solve((mu/m * K.tt + gamma * diag(m)),t(K.xt)))))
}

rkhs = function(xtrain, xtest, fullgrid,testlocations,D, mu,gamma,lengthscale,itrue) {
  Klist=my.kernel1(rbind(as.matrix(xtrain),as.matrix(xtest),testlocations),fullgrid,lengthscale,mu,gamma,20)
  ntrain = nrow(xtrain); ntest = nrow(xtest); n = ntrain + ntest
  alphastar = optim(rep(1,ntrain),function(alpha) ll.wrapper(alpha,Klist$K.xxreg[1:ntrain,1:ntrain],mu,1),
                    function(alpha) ll.grad(alpha,Klist$K.xxreg[1:ntrain,1:ntrain],mu,1),method="L-BFGS-B")
  
  ihat = mu*(t(alphastar$par) %*% Klist$K.xureg[1:ntrain,])^2
  #plot(ihat,itrue[samplelocations])
  
  ppl = -sum(log(mu * (Klist$K.xxreg[(ntrain+1):n,1:ntrain] %*% alphastar$par)^2)) + mean(ihat) 
  rmse = sqrt(mean((mu*(t(alphastar$par) %*% Klist$K.xxreg[1:ntrain,(ntrain+ntest+1):nrow(Klist$K.xxreg)])^2 - itrue)^2))
  return(data.frame(lengthscale=lengthscale,mu=mu,gamma=gamma,ll=alphastar$value,ppl=ppl,rmse=rmse))
}
rkhs.naive = function(xtrain, xtest, fullgrid, testlocations, D, mu,gamma,lengthscale,itrue,kernel=se.k) {
  K=kernel(as.matrix(dist(xtrain)),lengthscale)
  Kgrid=kernel(as.matrix(pdist(xtrain,fullgrid)),lengthscale)
  R = 1/nrow(fullgrid) * Kgrid %*% t(Kgrid)
  alphastar = optim(rep(1,nrow(K)),function(alpha) ll.naive(alpha,K,R,mu,gamma),
                    function(alpha) ll.grad.naive(alpha,K,R,mu,gamma),method="L-BFGS-B")
  ppl = (-sum(log(mu * (t(alphastar$par) %*% kernel(as.matrix(pdist(xtrain,xtest)),lengthscale))^2)) +
                     ( mu * t(alphastar$par) %*% R %*% alphastar$par))
  Ktest=kernel(as.matrix(pdist(xtrain,testlocations)),lengthscale)
  ihat = mu*(t(alphastar$par) %*% Ktest)^2
  rmse = sqrt(mean((ihat - itrue)^2))
  
  return(data.frame(lengthscale=lengthscale,mu=mu,gamma=gamma,ll=alphastar$value,ppl=ppl,rmse=rmse))
}
my.kde = function(xtrain,xtest,gridsize,fullgrid,testlocations,lengthscale,itrue) {
  n1 = nrow(xtest)
  n2 = n1 + nrow(fullgrid)
  eval.points = rbind(as.matrix(xtest),as.matrix(fullgrid),as.matrix(testlocations))
  n3 = nrow(eval.points)
  ihat = kde(xtrain,H=diag(lengthscale,D),eval.points=eval.points) #gridsize=gridsize,
  ihat$estimate = ihat$estimate * nrow(xtrain)
  ppl = -sum(log(ihat$estimate[1:n1])) + mean(ihat$estimate[(n1+1):n2])
  rmse = sqrt(mean((ihat$estimate[(n2+1):n3] - itrue)^2))
  return(data.frame(lengthscale=lengthscale,ppl=ppl,rmse=rmse))
}
ll.wrapper = function(a,b,c,d) {
  result = ll(a,b,c,d)
  if(!is.finite(result))
    result = 1e8
  return(result)
}

#set.seed(1)
xy.grid = seq(0,1,.01)
length(xy.grid)

fullgrid = replicate(D,runif(nsamplelocations))
testlocations = replicate(D,runif(ntestlocations))

overall.results = NULL

library("clusterGeneration")
library(mvtnorm)

gen.realization = function(f,D,imax,mu,means,covars) {
  ## number of random points
  npoints = rpois(1,imax)
  if(is.na(npoints) | npoints < 2 | npoints > 2000) {
    return(data.frame())
  }
  pp = data.frame(replicate(D,runif(npoints,0,1)))
  intensity.pp = mu*f(pp)^2
  keep = runif(npoints) <= intensity.pp / imax
  #  print(sprintf("max: %.02f",max(intensity.pp/imax)))
  pp = pp[keep,]
  return(pp)
}

significant = function(x,y) {
    (binom.test(sum(x < y), length(x))$p.value)
}

mus = mu
result = NULL
for(i in 1:nreps) {
    print(sprintf("##### %d (D=%d) ######",i,D))
    
    pp = list()
    while(1)  {
      delta = NULL; Sigma = NULL
      for(i in 1:k) {
        delta[[i]] = runif(D)
        Sigma[[i]] = .1 * genPositiveDefMat(D,"onion")$Sigma #diag(rexp(D)*.05)
       # Sigma[[i]][2,1] = Sigma[[i]][1,2] = runif(1,-.1,.1)
        
      }
      
      f = function(X) { 
        result = 0
        for(i in 1:k)
          result = result + (mu*dmvt(X,delta[[i]],Sigma[[i]],df,log=F)) 
        return(result)
      } 
      itrue = mu * f(testlocations)^2
      if(max(itrue) > 1000)
          cat('+')
      pp[[1]] = gen.realization(f,D,max(itrue),mu,means,covars)
      pp[[2]] = gen.realization(f,D,max(itrue),mu,means,covars)
      if(nrow(pp[[1]]) > 10 & nrow(pp[[2]]) > 10 & nrow(pp[[1]])< 500 & nrow(pp[[2]]) < 500) {
        break
      } else {
        if(nrow(pp[[1]]) + nrow(pp[[2]]) < 100)
          cat('-')
        else
          cat('+')
      }
    }
    
    n = nrow(pp[[1]])
    D = ncol(pp[[1]])
    fname = sprintf("debug/large-scale-experiments-studentt-k%d-df%d-D%s-mu%s-S%s.rdata",k,df,opts$D,opts$mu,opts$S)
    print(sprintf("saving image to: %s", fname))
    save.image(fname)

    params = expand.grid(mu=seq(5,400,50),gamma=c(.005,.01,.1,.5),lengthscale=c(.05,seq(.1,.8,.05)))
    traintest = expand.grid(train=1:2,test=1:2)
    traintest = traintest[traintest$train != traintest$test,]
    
    result.rkhs = foreach(ri = 1:nrow(traintest), .combine='rbind') %do% {
      foreach(j = 1:nrow(params), .combine='rbind')  %dopar% {
          ptm = proc.time()
        r = rkhs(pp[[traintest[ri,1]]], pp[[traintest[ri,2]]], fullgrid, testlocations, D, 
                 params$mu[j], params$gamma[j],params$lengthscale[j],itrue)
          
        r$elapsed = (proc.time() - ptm)[3]
       # print(sprintf("%.01f%% complete [%d] - %.02f",(j/nrow(params))*100,ri,r$elapsed))
        return(r)
      }
    }
    print(sprintf("rkhs mean elapsed time = %.02f sec",mean(result.rkhs$elapsed)))
    result.naive = foreach(ri = 1:nrow(traintest), .combine='rbind') %do% {
      foreach(j = 1:nrow(params), .combine='rbind')  %dopar% {
        ptm = proc.time()
        r = rkhs.naive(pp[[traintest[ri,1]]], pp[[traintest[ri,2]]], fullgrid, testlocations, D, 
                 params$mu[j], params$gamma[j],params$lengthscale[j],itrue)
        
        r$elapsed = (proc.time() - ptm)[3]
        # print(sprintf("%.01f%% complete [%d] - %.02f",(j/nrow(params))*100,ri,r$elapsed))
        return(r)
      }
    }
    print(sprintf("naive mean elapsed time = %.02f sec",mean(result.rkhs$elapsed)))
    result.kde = foreach(ri = 1:nrow(traintest), .combine='rbind') %dopar% {
      foreach(lengthscale = c(.05,seq(.1,.8,.05)), .combine='rbind')  %dopar% {
        r = my.kde(pp[[traintest[ri,1]]],pp[[traintest[ri,2]]],length(xy.grid),fullgrid,testlocations,lengthscale,itrue)
        return(r)
      }
    }
    r1=as.data.table(result.rkhs)[,list(ppl=mean(ppl),rmse=mean(rmse)),by=list(lengthscale,mu,gamma)]
    r1.naive=as.data.table(result.naive)[,list(ppl=mean(ppl),rmse=mean(rmse)),by=list(lengthscale,mu,gamma)]
    r2=as.data.table(result.kde)[,list(ppl=mean(ppl),rmse=mean(rmse)),by=list(lengthscale)]
    best.rkhs = r1[which.min(ppl)]
    best.kde = r2[which.min(ppl)]
    best.naive = r1.naive[which.min(ppl)]
    r1=as.data.table(result.rkhs)[lengthscale==best.rkhs$lengthscale & mu == best.rkhs$mu & gamma == best.rkhs$gamma]
    r1.naive=as.data.table(result.naive)[lengthscale==best.naive$lengthscale & mu == best.naive$mu & gamma == best.naive$gamma]
    r2=as.data.table(result.kde)[lengthscale==best.kde$lengthscale]
    result.new = data.frame(rkhs=mean(r1$rmse),naive=mean(r1.naive$rmse),kde=mean(r2$rmse)) #,significant=
    #result.new = data.frame(rkhs=mean(r1$ppl),naive=mean(r1.naive$ppl),kde=mean(r2$ppl)) #,significant=
    #t.test(r1$rmse,r2$rmse,paired=T)$p.value)
    print(result.new)

    fname = sprintf("results/large-scale-experiments-studentt-k%d-df%d-D%s-mu%s-S%s.csv",k,df,opts$D,opts$mu,opts$S)
    if(file.exists(fname)) {
      print(sprintf("results saved in %s",fname))
      result = read.csv(fname)
      print(sprintf("Fraction of times that RKHS is better than KDE (n=%d): %.02f [p-value: %f]",nrow(result),mean(result$rkhs < result$kde),
                    significant(result$rkhs,result$kde)))
      print(sprintf("Fraction of times that RKHS is better than naive: %.02f [p-value: %f]",mean(result$rkhs < result$naive),
                    significant(result$rkhs,result$naive)))
      print(sprintf("Fraction of times that naive is better than KDE: %.02f [p-value: %f]",mean(result$naive < result$kde),
                    significant(result$naive,result$kde)))
    }
    write.table(result.new,fname,row.names=F,append=T,col.names=!file.exists(fname),sep=",")
}
