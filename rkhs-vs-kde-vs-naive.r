library(data.table)
result = NULL

for(fname in list.files("results",pattern="gaussian-mixture-synthetic-experiments*",full.names=T)) {
  f = read.csv(fname)
  D = strsplit(fname,"D")[[1]][2]; D = strsplit(D,"-")[[1]][1]
  M = strsplit(fname,"M")[[1]][2]; M = strsplit(M,"-")[[1]][1]
  S = strsplit(fname,"S")[[1]][2];  S = strsplit(S,"[.]")[[1]][1]
  f$D=as.numeric(D); f$M=as.numeric(M); f$S=as.numeric(S)
  result = rbind(f,result)
}
result = data.table(result)
df = result[,list(fraction1=mean(rkhs < kde),fraction2=mean(rkhs < naive), 
                  fraction3 = mean(kde < naive),
                  rkhs=mean(rkhs),kde=mean(kde),naive=mean(naive),
                  n=length(kde)),
                  #p.value1=t.test(rkhs,kde,paired=T)$p.value,
                  #p.value2=t.test(rkhs,naive,paired=T)$p.value),
                  by=list(D,M,S)]
calc.se = function(p,n) {
  return(1.96 * sqrt(p*(1-p)/n))
}
df$fraction1.se = calc.se(df$fraction1,df$n)
df$fraction2.se = calc.se(df$fraction2,df$n)
df$fraction3.se = calc.se(df$fraction3,df$n)
#df = df[df$S == 1000,]
df = df[order(df$D),]

pdf("../../figures/rkhs-vs-kde-vs-naive-ejs.pdf",height=9,width=5)
par(mfrow=c(3,1), mai=c(.8,.82,.5,.82))
plot(df$D,df$fraction1*100,ylab="% of time RKHS outperforms KIE",
     xlab="Dimension", ty="l",ylim=c(0,105),xaxs="i",yaxs="i",xaxt="n",yaxt="n",main="(a)")
axis(1,labels=c(2,seq(3,15,2)),at=c(2,seq(3,15,2)))
axis(2,labels=seq(0,100,25),at=seq(0,100,25))
lines(df$D,(df$fraction1+df$fraction1.se)*100,col="gray")
lines(df$D,(df$fraction1-df$fraction1.se)*100,col="gray")
abline(h=50,col="darkblue")
#pdf("../../figures/rkhs-vs-naive.pdf",height=3.5,width=5)

#pdf("../../figures/kde-vs-naive.pdf",height=3.5,width=5)
plot(df$D,df$fraction2*100,ylab="% of time RKHS outperforms naive",
     xlab="Dimension", ty="l",ylim=c(0,105),xaxs="i",yaxs="i",xaxt="n",yaxt="n",main="(b)")
axis(1,labels=c(2,seq(3,15,2)),at=c(2,seq(3,15,2)))
axis(2,labels=seq(0,100,25),at=seq(0,100,25))
lines(df$D,(df$fraction2+df$fraction2.se)*100,col="gray")
lines(df$D,(df$fraction2-df$fraction2.se)*100,col="gray")
abline(h=50,col="darkblue")

plot(df$D,df$fraction3*100,ylab="% of time KIE outperforms naive",
     xlab="Dimension", ty="l",ylim=c(0,105),xaxs="i",yaxs="i",xaxt="n",yaxt="n",main="(c)")
axis(1,labels=c(2,seq(3,15,2)),at=c(2,seq(3,15,2)))
axis(2,labels=seq(0,100,25),at=seq(0,100,25))
lines(df$D,(df$fraction3+df$fraction3.se)*100,col="gray")
lines(df$D,(df$fraction3-df$fraction3.se)*100,col="gray")
abline(h=50,col="darkblue")


dev.off()
