## use the squared exponential Mercer expansion [Zhu et al 1997, Rasmussen and Williams 2006]

source("utils.r")
library(pdist)
library(data.table)
library("cvTools")
library(polynom)
library(truncnorm)
library(orthopolynom)
library(doMC)
registerDoMC()

hermite = hermite.h.polynomials( 64, normalized=F ) ## gives the physicists' hermite polynomials, 
sigma2 = .5
lengthscale= .5

pp.likelihood = function(K, R,mu,alpha) {
  (-sum(log(mu * (K %*% alpha)^2)) + ( mu * t(alpha) %*% R %*% alpha))
}


k.lambda = function(k,lengthscale) {
  a = 1/(4*sigma2^2)
  b = 1/(2*lengthscale^2)
  c = sqrt(a^2 + 2*a*b)
  A = a + b + c
  B = b/A
  (sqrt(2*a / A) * B^k)
}
k.phi = function(k,x,lengthscale) {
  a = 1/(4*sigma2^2)
  b = 1/(2*lengthscale^2)
  c = sqrt(a^2 + 2*a*b)
  A = a + b + c
  B = b/A
  1/sqrt( sqrt(a/c) * 2^k * factorial(k)) * 
    (exp(-(c-a)*x^2) * predict(hermite[[k+1]],sqrt(2*c)*x))
}
### generate a synthetic intensity in H_k
set.seed(9)
a = rnorm(length(hermite))
gridwidth = .02
u = seq(gridwidth * .5,1,.01)
lengthscale = .1
f = Reduce('+', lapply(0:(length(hermite)-1), function(i) a[i+1] * sqrt(k.lambda(i,lengthscale)) * k.phi(i,u,lengthscale)))
mu=400
intensity = mu*f^2
plot(u,intensity,ty="l")
imax = max(intensity)
pp = list()
for(k in 1:3) { # generate realizations
  npoints = rpois(1,imax )
  pp[[k]] = runif(npoints,0,1)
  pp[[k]] = pp[[k]][order(pp[[k]])]

  f.pp = Reduce('+', lapply(0:(length(hermite)-1), function(i) a[i+1] * sqrt(k.lambda(i,lengthscale)) * k.phi(i,pp[[k]],lengthscale)))
  pp.intensity = mu*f.pp^2
  plot(u,intensity,ty="l")
  lines(pp[[k]],pp.intensity,col="red")
  retain = runif(length(pp[[k]])) < (pp.intensity/imax)
  pp[[k]] = pp[[k]][retain]
  print(length(pp[[k]]))
}

smooth = density(pp[[1]],"UCV")
print(smooth$bw)
plot(u,intensity,col="blue",ty="l")
lines(smooth$x,smooth$y*length(pp[[1]]),ty="l",ylim=c(0,30))

rug(pp[[1]])

cv = function(xtrain, xtest, grid, mu,gamma,lengthscale,kernel, r.kernel, itrue) {
  K=kernel(c(xtrain,xtest),grid,lengthscale,mu,gamma)
  ntrain = length(xtrain); ntest = length(xtest); n = ntrain + ntest
  alphastar = optim(rep(1,ntrain),function(alpha) ll(alpha,K[1:ntrain,1:ntrain],mu,1),
                    function(alpha) ll.grad(alpha,K[1:ntrain,1:ntrain],mu,1),method="L-BFGS-B")
  Kgrid = se.kxureg.nystrom(xtrain,grid,lengthscale,mu,gamma)
  ihat = mu * (t(alphastar$par) %*% Kgrid)^2
  mse = mean((ihat - itrue)^2)
  test.ppl = -sum(log(mu * (K[(ntrain+1):n,1:ntrain] %*% alphastar$par)^2)) + mean(ihat) 
  
  return(data.frame(lengthscale=lengthscale,mu=mu,gamma=gamma,ll=alphastar$value,test.ppl=test.ppl,mse=mse))
}
cv.naive = function(dd_train, dd_traintest, dd_traingrid, mu,gamma,lengthscale,kernel,itrue) {
  K=kernel(dd_train,lengthscale)
  Kgrid=kernel(dd_traingrid,lengthscale)
  R = 1/length(grid) * Kgrid %*% t(Kgrid) 
  alphastar = optim(rep(1,nrow(dd_train)),function(alpha) ll.naive(alpha,K,R,mu,gamma),
                    function(alpha) ll.grad.naive(alpha,K,R,mu,gamma),method="L-BFGS-B")
  test.ppl = pp.likelihood(t(kernel(dd_traintest,lengthscale)), R,mu,matrix(alphastar$par))
  mse = mean((mu * (t(alphastar$par) %*% Kgrid)^2 - itrue)^2)
  
  return(data.frame(lengthscale=lengthscale,mu=mu,gamma=gamma,ll=alphastar$value,test.ppl=test.ppl,mse=mse))
}
normalized.se.k = function(dd,lengthscale) se.k(dd,lengthscale) / (sqrt(2*pi)*lengthscale)

my.kde = function(xtrain, xtest,grid,lengthscale,itrue) {
  dd = as.matrix(pdist(matrix(xtrain),matrix(c(xtest,grid))))
  K = t(apply(dd,1,function(x) dtruncnorm(x,sd=lengthscale)))
  n1 = length(xtest); n = ncol(K) 
  ihat = colSums(K[,(n1+1):n])
  test.ppl = -sum(log(colSums(K[,1:n1]))) + mean(ihat)
  mse = mean((ihat - itrue)^2)
  return(data.frame(lengthscale=lengthscale,test.ppl=test.ppl,mse=mse))
}

grid=u
folds = cvFolds(length(pp),2,R=5)
ptm = proc.time()
params = expand.grid(mu=c(2,5,10,50,100,150,200), gamma=c(.1,.5,1,2), lengthscale=c(.01,.02,.05,.1,.2, .5))
traintest = expand.grid(train=1:3,test=1:3)
traintest = traintest[traintest$train != traintest$test,]
result = foreach(i = 1:nrow(traintest), .combine='rbind')  %do% {
    train = traintest$train[i]
    test = traintest$test[i]
    result1 = foreach(j = 1:nrow(params), .combine='rbind', .errorhandling='pass')  %dopar% {
      r = cv(pp[[train]], pp[[test]], grid, params$mu[j], params$gamma[j],params$lengthscale[j], se.kreg.nystrom, se.rreg.nystrom, intensity)
      
      print(r)
      return(r)
    }
    result1$train = train
    result1$test = test
    return(result1)
}
proc.time() - ptm

result = data.table(result)
result1 = result[,list(test.ppl=mean(test.ppl),mse=mean(mse),se=sd(test.ppl)/sqrt(length(test.ppl))),by=list(lengthscale,mu,gamma)]
best = result1[which.min(test.ppl)]
best

lengthscale=best$lengthscale
mu=best$mu
gamma = best$gamma
Kreg = se.kreg.nystrom(pp[[1]],grid,lengthscale,mu,gamma)
Kreg.predict = se.kxureg.nystrom(pp[[1]],u,best$lengthscale,best$mu,best$gamma)
 
alphastar = optim(rep(1,length(pp[[1]])),function(alpha) ll(alpha,Kreg,mu),
                   function(alpha) ll.grad(alpha,Kreg,mu),method="L-BFGS-B",control=list(maxit=1000))
ihat.reg=mu*(t(Kreg.predict) %*% alphastar$par)^2
lines(u,ihat.reg,ty="l",col="red")

result = foreach(i = 1:nrow(traintest), .combine='rbind') %do% {
  train = traintest$train[i]
  test = traintest$test[i]
  result1 = foreach(j = 1:nrow(params), .combine='rbind', .errorhandling='pass')  %dopar% {
    r = cv.naive(outer(pp[[train]],pp[[train]],"-"), outer(pp[[train]],pp[[test]],"-"), outer(pp[[train]],u,"-"), 
                 params$mu[j], params$gamma[j],params$lengthscale[j], se.k,intensity)
    
    return(r)
  }
  result1$k = i
  return(result1)
}

proc.time() - ptm
result = data.table(result)
result2 = result[,list(test.ppl=mean(test.ppl),se=sd(test.ppl)/sqrt(length(test.ppl)),mse=mean(mse)),
                 by=list(lengthscale,mu,gamma)]
best.2 = result2[which.min(test.ppl)]
best.2

K = with(best.2,se.k(outer(pp[[1]],pp[[1]],"-"),lengthscale))
Kgrid=with(best.2,se.k(outer(pp[[1]],u,"-"),lengthscale))
R = 1/length(grid) * Kgrid %*% t(Kgrid) 

alphastar = optim(rep(1,length(pp[[1]])),function(alpha) ll.naive(alpha,K,R,best.2$mu,best.2$gamma),
                  function(alpha) ll.grad.naive(alpha,K,R,best.2$mu,best.2$gamma),method="L-BFGS-B",control=list(maxit=1000))
ihat.2=best.2$mu*(t(Kgrid) %*% alphastar$par)^2

# KDE
result = foreach(i = 1:nrow(traintest), .combine='rbind') %do% {
  train = traintest$train[i]
  test = traintest$test[i]
  result1 = foreach(lengthscale = unique(params$lengthscale), .combine='rbind', .errorhandling='pass')  %dopar% {
    r = my.kde(pp[[train]], pp[[test]], grid, lengthscale, intensity)
    
    #  print(r)
    return(r)
  }
  result1$k = i
  return(result1)
}

result = data.table(result)
result.kde = result[,list(test.ppl=mean(test.ppl),se=sd(test.ppl)/sqrt(length(test.ppl)),mse=mean(mse)),by=list(lengthscale)]
best.kde = result.kde[which.min(test.ppl)]

dd = as.matrix(pdist(matrix(pp[[1]]),matrix(grid)))
K = t(apply(dd,1,function(x) dtruncnorm(x,sd=best.kde$lengthscale)))

ihat.kde = colSums(K)
plot(u,intensity,col="black",ty="l",xlab="x",ylim=c(0,700))
lines(u,ihat.kde,ty="l",col="blue")

pdf("figures/synthetic-figure-1.pdf",width=6,height=6)
plot(u,intensity,col="black",ty="l",xlab="x")
lines(u,ihat.kde,ty="l",col="blue")
lines(u,ihat.reg,ty="l",col="red")
lines(u,ihat.2,col="green")
rug(pp[[1]])
legend("topright",c("true","kernel smoothed","RKHS","RKHS (naive)"),fill=c("black","blue","red","green"))
dev.off()
## RMSE:
sqrt(mean((ihat.kde-intensity)^2))
sqrt(mean((ihat.reg-intensity)^2))
sqrt(mean((ihat.2-intensity)^2))
