library(data.table)
result = NULL

for(fname in list.files("results",pattern="large-scale-experiments-studentt-k20-df5-D*",full.names=T)) {
  f = read.csv(fname)
  D = strsplit(fname,"D")[[1]][2]; D = strsplit(D,"-")[[1]][1]
  mu = strsplit(fname,"mu")[[1]][2]; mu = strsplit(mu,"-")[[1]][1]
  S = strsplit(fname,"S")[[1]][2];  S = strsplit(S,"[.]")[[1]][1]
  f$D=as.numeric(D); f$mu=as.numeric(mu); f$S=as.numeric(S)
  result = rbind(f,result)
}
result = data.table(result)
df = result[,list(fraction1=mean(rkhs < kde),fraction2=mean(rkhs < naive), 
                  fraction3 = mean(kde < naive),
                  rkhs=mean(rkhs),kde=mean(kde),naive=mean(naive),
                  n=length(kde)),
            #p.value1=t.test(rkhs,kde,paired=T)$p.value,
            #p.value2=t.test(rkhs,naive,paired=T)$p.value),
            by=list(D,S)]
calc.se = function(p,n) {
  return(1.96 * sqrt(p*(1-p)/n))
}
df$fraction1.se = calc.se(df$fraction1,df$n)
df$fraction2.se = calc.se(df$fraction2,df$n)
df$fraction3.se = calc.se(df$fraction3,df$n)
#df = df[df$S == 1000,]
df = df[order(df$D),]


pdf("pct-improvement-studentt.pdf",width=8.5,height=5)
par(mfrow=c(1,3))
f = result[D == 8]
hist(100*(1-f$rkhs / f$kde),breaks=32,xlab="% improvement of RKHS over KIE (dimension = 8)",main="(a)")
hist(100*(1-f$rkhs / f$naive),breaks=32,xlab="% improvement of RKHS over naive  (dimension = 8)",main="(b)")
hist(100*(1-f$kde / f$naive),breaks=32,xlab="% improvement of KIE over naive  (dimension = 8)",main="(c)")
dev.off()

pdf("rkhs-vs-kde-vs-naive-studentt.pdf",height=9,width=5)
par(mfrow=c(3,1), mai=c(.8,.82,.5,.82))
plot(df$D,df$fraction1*100,ylab="% of time RKHS outperforms KIE",
     xlab="Dimension", ty="l",ylim=c(0,105),xaxs="i",yaxs="i",xaxt="n",yaxt="n",main="(e)")
axis(1,labels=c(2,seq(3,25,2)),at=c(2,seq(3,25,2)))
axis(2,labels=seq(0,100,25),at=seq(0,100,25))
lines(df$D,(df$fraction1+df$fraction1.se)*100,col="gray")
lines(df$D,(df$fraction1-df$fraction1.se)*100,col="gray")
abline(h=50,col="darkblue")
plot(df$D,df$fraction2*100,ylab="% of time RKHS outperforms naive",
     xlab="Dimension", ty="l",ylim=c(0,105),xaxs="i",yaxs="i",xaxt="n",yaxt="n",main="(f)")
axis(1,labels=c(2,seq(3,25,2)),at=c(2,seq(3,25,2)))
axis(2,labels=seq(0,100,25),at=seq(0,100,25))
lines(df$D,(df$fraction2+df$fraction2.se)*100,col="gray")
lines(df$D,(df$fraction2-df$fraction2.se)*100,col="gray")
abline(h=50,col="darkblue")

plot(df$D,df$fraction3*100,ylab="% of time KIE outperforms naive",
     xlab="Dimension", ty="l",ylim=c(0,105),xaxs="i",yaxs="i",xaxt="n",yaxt="n",main="(g)")
axis(1,labels=c(2,seq(3,25,2)),at=c(2,seq(3,25,2)))
axis(2,labels=seq(0,100,25),at=seq(0,100,25))
lines(df$D,(df$fraction3+df$fraction3.se)*100,col="gray")
lines(df$D,(df$fraction3-df$fraction3.se)*100,col="gray")
abline(h=50,col="darkblue")

dev.off()
