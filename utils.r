library(mgcv)
se.kreg.nystrom = function(x,u,lengthscale,mu,gamma) {
  K.uu = se.k(outer(u,u,"-"),lengthscale)
  K.xu = se.k(outer(x,u,"-"),lengthscale)
  e = eigen(K.uu) # for a low rank version use: slanczos(K.uu,rank)
  m = length(u)
  return(K.xu %*% e$vectors %*% diag(1/(mu/m * e$values^2 + gamma * e$values))%*% t(e$vectors) %*% t(K.xu))
}
se.rreg.nystrom = function(x,u,lengthscale,mu,gamma) {
  K = se.kxureg.nystrom(x,u,lengthscale,mu,gamma)
  m = length(u)
  return(1/m * K %*% t(K))
}

se.kxureg.nystrom = function(x,u,lengthscale,mu,gamma) {
  K.uu = se.k(outer(u,u,"-"),lengthscale)
  K.xu = se.k(outer(x,u,"-"),lengthscale)
  m = length(u)
  return(t(solve((mu/m * K.uu + gamma * diag(m)),t(K.xu))))
}

se.k = function(dd,lengthscale) {
  return(exp(-.5*dd^2/lengthscale^2))
}

ll = function(alpha,K,mu,gamma=1) {
  (-sum(log(mu * (K %*% alpha)^2)) + gamma * t(alpha) %*% K %*% alpha)
}

ll.grad = function(alpha,K,mu,gamma=1) {
  (-2 * rowSums(t((t(K)/as.numeric( K%*%alpha )))) + 2 * gamma * K %*% alpha)
}

ll.naive = function(alpha,K,R,mu,gamma=1) {
  (-sum(log(mu * (K %*% alpha)^2)) + gamma * t(alpha) %*% K %*% alpha + 
     mu * t(alpha) %*% R %*% alpha)
}

ll.grad.naive = function(alpha,K,R,mu,gamma=1) {
  (-2 * rowSums(t((t(K)/as.numeric( K%*%alpha )))) + 2 * gamma * K %*% alpha + 
     2 * mu * R %*% alpha)
}
sem <- function(x) sd(x)/sqrt(length(x))
my.kernel = function(x,u,grid,lengthscale,mu,gamma,lowrank=10,kernel=se.k) {
      K.uu = kernel(as.matrix(dist(u)),lengthscale)
  K.grid = kernel(as.matrix(dist(grid)),lengthscale)
    K.xu = kernel(as.matrix(pdist(x,u)),lengthscale)
    K.xgrid = kernel(as.matrix(pdist(x,grid)),lengthscale)
      #e = eigen(K.uu) #
      e = slanczos(K.uu,lowrank)
      m = nrow(u)
        return(list(K.xxreg=K.xu %*% e$vectors %*% diag(1/(mu/m * e$values^2 + gamma * e$values))%*% t(e$vectors) %*% t(K.xu),
                                  K.xureg=t(solve((mu/nrow(grid) * K.grid + gamma * diag(nrow(grid))),t(K.xgrid)))))
}
