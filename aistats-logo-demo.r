library(jpeg)
img = readJPEG("aistats-logo-bw.jpg")

rotate <- function(x) t(apply(x, 2, rev))

a = rotate(img[,,1]+img[,,2]+img[,,3])
image(rotate(a)) 
X = a
n = nrow(which(X <= 2.8,arr.ind=T))
a[which(X > 2.8,arr.ind=T)] = 0
a[which(X <= 2.8,arr.ind=T)] = runif(n,4000,8000) 
image(a)

X = a
image(X)
imax=max(X) 

npoints = rpois(1,imax )
print(npoints)
xn = nrow(X)
yn = ncol(X)
pp = data.frame(x=sample(xn,npoints,replace=T),y=sample(yn,npoints,replace=T))
pp.intensity = apply(pp,1,function(i) X[i[1],i[2]])
retain = runif(nrow(pp)) < (pp.intensity/imax)


library(spatstat)
plot(density(data.matrix(pp[retain,])))
points = data.matrix(pp)
points[,1] = (points[,1] - min(points[,1])) / max(points[,1])
points[,2] = (points[,2] - min(points[,2])) / max(points[,2])
write.csv(points[retain,],"aistats-logo-xy.csv",row.names=F)
ppx = as.ppp(jitter(points[retain,]),square(1))
ppx0 = as.ppp(jitter(points[!retain,]),square(1))
plot(ppx)
png("aistats-pp.png",width=203*2,height=90*2)

par(mar=c(0,0,0,0))
plot(as.data.frame(ppx),col=rgb(0,0,0,.5),bty="n",axes=F,xlab="",ylab="",cex=.7)
dev.off()

pdf("aistats-pp-slides.pdf",width=10,height=4*1.125) #,width=203*2,height=90*2)
par(bg=NA) 
 #par(bg = rgb(4,30,150,max=255)) # for presentation, background is oxford blue
par(mar=c(0,0,0,0))
plot(as.data.frame(ppx),col=rgb(1,1,1,.75),bty="n",axes=F,xlab="",ylab="",cex=.7)
dev.off()
