library(docopt)
library(data.table)
library("cvTools")
library(doMC)
registerDoMC()
library(pdist)
source("utils.r")

'Usage:
tree-datasets-in-spatstat.r [--dataset dataset] [--S samplelocations]

Options:
--dataset dataset [default: whiteoak]
--S samplelocations [default: 200]
]' -> doc

opts <- docopt(doc)

### load tree data
library(spatstat)
my.likelihood = function(K, R,mu,alpha) {
  (-sum(log(mu * (K %*% alpha)^2)) + ( mu * t(alpha) %*% R %*% alpha))
}

unitsquare = function(x) {
  (x - min(x)) / (max(x) - min(x))
}

data(lansing)

if(!opts$dataset %in% names(split(lansing))) {
  data(list=opts$dataset)
  tree.pp = eval(parse(text=opts$dataset))
} else {
  tree.pp = split(lansing)[[opts$dataset]]
}
tree = opts$dataset

results = NULL
pp = cbind(x=unitsquare(tree.pp$x),y=unitsquare(tree.pp$y))
#write.table(pp,sprintf("data/%s-enviro.csv",tree), row.names=F,col.names=F,sep=",")

n = nrow(pp)
gridwidth = .05
u = seq(gridwidth*.5,1,gridwidth)
xy.grid = as.matrix(expand.grid(u,u))
xy.grid = xy.grid[,ncol(xy.grid):1]

nsamplelocations = as.numeric(opts$S)
samplelocations = replicate(2,runif(nsamplelocations))

ihat.grid = density(tree.pp,bw.diggle(tree.pp))

ll.wrapper = function(a,b,c,d) {
  result = ll(a,b,c,d)
  if(!is.finite(result))
    result = 1e8
  return(result)
}

my.kernel = function(x,u,grid,lengthscale,mu,gamma,lowrank=10,kernel=se.k) {
  K.uu = kernel(as.matrix(dist(u)),lengthscale)
  K.grid = kernel(as.matrix(dist(grid)),lengthscale)
  K.xu = kernel(as.matrix(pdist(x,u)),lengthscale)
  K.xgrid = kernel(as.matrix(pdist(x,grid)),lengthscale)
  #e = eigen(K.uu) #
  e = slanczos(K.uu,lowrank)
  m = nrow(u)
  return(list(K.xxreg=K.xu %*% e$vectors %*% diag(1/(mu/m * e$values^2 + gamma * e$values))%*% t(e$vectors) %*% t(K.xu),
              K.xureg=t(solve((mu/nrow(grid) * K.grid + gamma * diag(nrow(grid))),t(K.xgrid)))))
}

cv = function(xtrain, xtest, xy.grid, samplelocations, mu,gamma,lengthscale) {
  Klist=my.kernel(rbind(xtrain,xtest), samplelocations, xy.grid, lengthscale,mu,gamma,20)
  ntrain = nrow(xtrain); ntest = nrow(xtest); n = ntrain + ntest
  alphastar = optim(rep(1,ntrain),function(alpha) ll.wrapper(alpha,Klist$K.xxreg[1:ntrain,1:ntrain],mu,1),
                    function(alpha) ll.grad(alpha,Klist$K.xxreg[1:ntrain,1:ntrain],mu,1),method="L-BFGS-B")
  
  test.ppl = -sum(log(mu * (Klist$K.xxreg[(ntrain+1):n,1:ntrain] %*% alphastar$par)^2)) +  
     mu * sum((t(alphastar$par) %*% Klist$K.xureg[1:ntrain,])^2) / nrow(xy.grid) 
  
  return(data.frame(lengthscale=lengthscale,mu=mu,gamma=gamma,ll=alphastar$value,
                    test.ppl=test.ppl))
}
cv.naive = function(dd_train, dd_traintest, dd_traingrid, mu,gamma,lengthscale,kernel) {
  K=kernel(dd_train,lengthscale)
  Kgrid=kernel(dd_traingrid,lengthscale)
  R = 1/nrow(xy.grid) * Kgrid %*% t(Kgrid) 
  alphastar = optim(rep(1,nrow(dd_train)),function(alpha) ll.naive(alpha,K,R,mu,gamma),
                    function(alpha) ll.grad.naive(alpha,K,R,mu,gamma),method="L-BFGS-B")
  test.ppl = my.likelihood(t(kernel(dd_traintest,lengthscale)), R,mu,matrix(alphastar$par))
  return(data.frame(lengthscale=lengthscale,mu=mu,gamma=gamma,ll=alphastar$value,test.ppl=test.ppl))
}

set.seed(1) # for reproducibility
folds = cvFolds(nrow(pp),2,R=2)
ptm = proc.time()

params = expand.grid(mu=c(.05,.1,.2,.5,1,2,3,4,5,8,10,50,75,100,200),gamma=c(.05,.1,.25,.5,1),
                     lengthscale=c(seq(.01,.04,.01),seq(.05,1,.1)))
result.rkhs = foreach(i = 1:folds$K, .combine='rbind') %do% {
  foreach(ri = 1:ncol(folds$subsets),.combine='rbind') %do% {
    train = folds$subsets[folds$which != i,ri]
    test = folds$subsets[folds$which == i,ri]
    
    result1 = foreach(j = 1:nrow(params), .combine='rbind')  %dopar% {
      r = cv(pp[train,], pp[test,], xy.grid, samplelocations, params$mu[j], params$gamma[j],params$lengthscale[j])
      r$k = i
      return(r)
    }
    
    return(result1)
  }
}

result.naive = foreach(i = 1:folds$K, .combine='rbind') %do% {
  foreach(ri = 1:ncol(folds$subsets),.combine='rbind') %do% {
    train = folds$subsets[folds$which != i,ri]
    test = folds$subsets[folds$which == i,ri]

    result1 = foreach(j = 1:nrow(params), .combine='rbind', .errorhandling='pass')  %dopar% {
      r = cv.naive(as.matrix(dist(pp[train,])), as.matrix(pdist(pp[train,],pp[test,])), as.matrix(pdist(pp[train,],xy.grid)),
                   params$mu[j], params$gamma[j],params$lengthscale[j], se.k)
      r$k = i
      return(r)
    }
    return(result1)
  }
}
result.kde = foreach(i = 1:folds$K, .combine='rbind') %do% {
  foreach(ri = 1:ncol(folds$subsets),.combine='rbind') %do% {
    train = folds$subsets[folds$which != i,ri]
    test = folds$subsets[folds$which == i,ri]
    pp_train = as.ppp(pp[train,],square(1))
    pp_test = as.ppp(pp[test,],square(1))
    
    result1 = foreach(lengthscale = unique(params$lengthscale), .combine='rbind', .errorhandling='pass')  %dopar% {
      #      b = params$lengthscale[j] #bw.ppl(pp_train)
      ihat.grid = density(pp_train,lengthscale,eps=u[2]-u[1],edge=T)
      idx = nearest.raster.point(pp[test,1],pp[test,2],ihat.grid)
      
      ihat.test = vapply(1:length(idx$row), function(i) ihat.grid[idx$row[i],idx$col[i]], 0)
      if(any(ihat.test <= 0)) {
        return(data.frame(test.ppl=NA,lengthscale=as.numeric(lengthscale),k=i,ri=ri,zero=sum(ihat.test == 0)))
      } else { 
        kde.ppl = -sum(log(ihat.test)) + (sum(ihat.grid) * ihat.grid$xstep * ihat.grid$ystep)
        
        return(data.frame(test.ppl=kde.ppl,lengthscale=as.numeric(lengthscale),k=i,ri=ri,zero=0))
      }
    }
  }
}
print("Done running... saving results to debug")
save(result.rkhs,result.kde,result.naive,file="Debug.rdata")
result.rkhs = data.table(result.rkhs)
result.rkhs.avg = result.rkhs[,list(test.ppl=mean(test.ppl)),by=list(lengthscale,mu,gamma)]
best.rkhs = result.rkhs.avg[which.min(test.ppl)]
best.rkhs
result.rkhs.best = result.rkhs[lengthscale==best.rkhs$lengthscale & mu==best.rkhs$mu & gamma == best.rkhs$gamma]

result.kde = data.table(result.kde)[complete.cases(result.kde),]
result.kde.avg = result.kde[,list(test.ppl=mean(test.ppl)),by=lengthscale]
best.kde = result.kde.avg[which.min(test.ppl)]
best.kde
result.kde.best = result.kde[lengthscale==best.kde$lengthscale]

result.naive = data.table(result.naive)
result.naive.avg = result.naive[,list(test.ppl=mean(test.ppl),se=sd(test.ppl)/sqrt(length(test.ppl))),by=list(lengthscale,mu,gamma)]
best.naive = result.naive.avg[which.min(test.ppl)]
best.naive
result.naive.best = result.naive[lengthscale==best.naive$lengthscale & mu==best.naive$mu & gamma == best.naive$gamma]

results = rbind(results,
                data.frame(tree=tree,
                           lik.kde=best.kde$test.ppl,
                           se.kde=sem(result.kde$test.ppl),
                           lik.naive=best.naive$test.ppl,
                           se.naive=sem(result.naive.best$test.ppl),
                           lik.rkhs=best.rkhs$test.ppl,
                           se.rkhs=sem(result.rkhs.best$test.ppl),
                           kde.vs.naive=t.test(result.kde.best$test.ppl,result.naive.best$test.ppl,paired=T)$p.value,
                           kde.vs.rkhs=t.test(result.kde.best$test.ppl,result.rkhs.best$test.ppl,paired=T)$p.value,
                           naive.vs.rkhs=t.test(result.naive.best$test.ppl,result.rkhs.best$test.ppl,paired=T)$p.value
                ))
print(results[nrow(results),])
write.table(results,sprintf("results/%s-enviro.csv",tree),sep=",",col.names=F)
save(best.rkhs,best.kde,best.naive,results,file=sprintf("results/%s-enviro.rdata",tree))
