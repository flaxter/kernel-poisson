# "Poisson intensity estimation with reproducing kernels" [AISTATS 2017] #

### Authors: Seth Flaxman, Yee Whye Teh, and Dino Sejdinovic ###


![aistats logo point pattern](https://bitbucket.org/repo/k8Lk9g/images/244542920-aistats-pp.png)


AISTATS proceedings version: http://proceedings.mlr.press/v54/flaxman17a/flaxman17a.pdf

arXiv: https://arxiv.org/abs/1610.08623