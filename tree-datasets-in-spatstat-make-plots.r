library(pdist)
source("utils.r")
library(spatstat)
data(lansing)
pp = split(lansing)[["whiteoak"]]
load("results/whiteoak-enviro.rdata")

gridwidth = .01
u = seq(gridwidth*.5,1,gridwidth)
xy.grid = as.matrix(expand.grid(u,u))
xy.grid = xy.grid[,ncol(xy.grid):1]
xy = data.matrix(as.data.frame(pp))

ptm = proc.time()
Klist=my.kernel(xy, xy.grid[sample(nrow(xy.grid),400),], xy.grid, best.rkhs$lengthscale,best.rkhs$mu,best.rkhs$gamma,20)
proc.time() - ptm

ntrain = nrow(xy); 
alphastar = optim(rep(1,ntrain),function(alpha) ll.wrapper(alpha,Klist$K.xxreg[1:ntrain,1:ntrain],best.rkhs$mu,1),
                  function(alpha) ll.grad(alpha,Klist$K.xxreg[1:ntrain,1:ntrain],best.rkhs$mu,1),method="L-BFGS-B")

ihat.rkhs = best.rkhs$mu*(t(alphastar$par) %*% Klist$K.xureg[1:ntrain,])^2
png(("figures/whiteoak-rkhs.png"),width=400,height=400)
image(t(matrix(ihat.rkhs,length(u))))
points(xy,pch=20,col=rgb(.2,.2,.2,1))
dev.off()

png(("figures/whiteoak-kde.png"),width=400,height=400)
ihat.grid = density(pp,b=best.kde$lengthscale,edge=T,eps=gridwidth)
image(t(ihat.grid$v))
points(xy,pch=20,col=rgb(.2,.2,.2,1))
dev.off()
png(("figures/whiteoak-kde-noedge.png"),width=400,height=400)
ihat.grid = density(pp,b=best.kde$lengthscale,edge=F,eps=gridwidth)
image(t(ihat.grid$v))
points(xy,pch=20,col=rgb(.2,.2,.2,1))
dev.off()

K=se.k(as.matrix(dist(xy)),best.naive$lengthscale)
Kgrid=se.k(as.matrix(pdist((xy),xy.grid)),best.naive$lengthscale)
R = 1/nrow(xy.grid) * Kgrid %*% t(Kgrid) 
alphastar = optim(rep(1,nrow(K)),function(alpha) ll.naive(alpha,K,R,best.naive$mu,best.naive$gamma),
                  function(alpha) ll.grad.naive(alpha,K,R,best.naive$mu,best.naive$gamma),method="L-BFGS-B")
ihat.naive = best.naive$mu * (as.numeric(t(matrix(alphastar$par)) %*% Kgrid))^2

png(("figures/whiteoak-naive.png"),width=400,height=400)
image(t(matrix(ihat.naive,length(u))))
points(pp,pch=20,col=rgb(.2,.2,.2,1))
dev.off()
